<gateway compatibility="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.itrsgroup.com/GA4.1.0-170114/gateway.xsd">
    <probes>
        
        
    </probes>
    <managedEntities>
        <managedEntity name="AppDev_Anne">
            <probe ref="AppDev"></probe>
            <addTypes>
                <type ref="GeneralCheck_A"></type>
            </addTypes>
        </managedEntity>
        <managedEntity name="ELK_Anne">
            <probe ref="ELK"></probe>
            <addTypes>
                <type ref="GeneralCheck_A"></type>
            </addTypes>
        </managedEntity>
        <managedEntity name="uDeploy_Anne">
            <probe ref="uDeploy"></probe>
            <addTypes>
                <type ref="GeneralCheck_A"></type>
            </addTypes>
            <sampler ref="Processes"></sampler>
        </managedEntity>
    </managedEntities>
    <types>
        <type name="GeneralCheck_A">
            <sampler ref="CPU A"></sampler>
            <sampler ref="Root Disk A"></sampler>
            <sampler ref="Check Ex Permission A"></sampler>
            <sampler ref="Big files"></sampler>
            <sampler ref="Network A"></sampler>
        </type>
        <type name="DB Samplers">
            <sampler ref="DB Exists"></sampler>
            <sampler ref="DB up"></sampler>
        </type>
    </types>
    <samplers>
        <sampler name="CPU A">
            <var-group>
                <data></data>
            </var-group>
            <plugin>
                <cpu></cpu>
            </plugin>
        </sampler>
        <sampler name="Root Disk A">
            <plugin>
                <disk>
                    <partitions>
                        <partition>
                            <path>
                                <data>/</data>
                            </path>
                        </partition>
                    </partitions>
                </disk>
            </plugin>
        </sampler>
        <sampler name="Check Ex Permission A">
            <plugin>
                <toolkit>
                    <samplerScript>
                        <data>/bin/ls -lrt / | /bin/grep &apos;rwx&apos; | /usr/bin/awk &apos;BEGIN {OFS=&quot;,&quot;; print &quot;Filename&quot;,&quot;Owner&quot;}{print $9,$3}&apos;</data>
                    </samplerScript>
                </toolkit>
            </plugin>
        </sampler>
        <sampler name="Processes up">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>ssh</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
        <sampler name="DB Exists">
            <plugin>
                <sql-toolkit>
                    <queries>
                        <query>
                            <name>
                                <data>QueryA</data>
                            </name>
                            <sql>
                                <data>select * from table_name</data>
                            </sql>
                            <rowLimit>
                                <data>1</data>
                            </rowLimit>
                        </query>
                    </queries>
                    <connection>
                        <database>
                            <mysql></mysql>
                        </database>
                    </connection>
                </sql-toolkit>
            </plugin>
        </sampler>
        <sampler name="Network A">
            <plugin>
                <network></network>
            </plugin>
        </sampler>
        <sampler name="Processes">
            <plugin>
                <processes>
                    <processes>
                        <process>
                            <data>
                                <alias>
                                    <data>ssh</data>
                                </alias>
                            </data>
                        </process>
                    </processes>
                </processes>
            </plugin>
        </sampler>
    </samplers>
    <rules>
        <rule name="Execute Permission">
            <targets>
                <target>/geneos/gateway[(@name=&quot;student01&quot;)]/directory/probe[(@name=&quot;ELK&quot;)]/managedEntity[(@name=&quot;ELK_Anne&quot;)]/sampler[(@name=&quot;Check Ex Permission A&quot;)][(@type=&quot;GeneralCheck_A&quot;)]/dataview[(@name=&quot;Check Ex Permission A&quot;)]/rows/row/cell[(@column=&quot;Owner&quot;)]</target>
            </targets>
            <priority>1</priority>
            <block>
                <if>
                    <equal>
                        <dataItem>
                            <property>@value</property>
                        </dataItem>
                        <string>root</string>
                    </equal>
                    <transaction>
                        <update>
                            <property>state/@severity</property>
                            <severity>warning</severity>
                        </update>
                    </transaction>
                    <transaction>
                        <update>
                            <property>state/@severity</property>
                            <severity>ok</severity>
                        </update>
                    </transaction>
                </if>
            </block>
        </rule>
        <rule name="Root PercentUsed">
            <targets>
                <target>/geneos/gateway[(@name=&quot;student01&quot;)]/directory/probe[(@name=&quot;ELK&quot;)]/managedEntity[(@name=&quot;ELK_Anne&quot;)]/sampler[(@name=&quot;Root Disk A&quot;)][(@type=&quot;GeneralCheck_A&quot;)]/dataview[(@name=&quot;Root Disk A&quot;)]/rows/row[(@name=&quot;/&quot;)]/cell[(@column=&quot;percentageUsed&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;student01&quot;)]/directory/probe[(@name=&quot;AppDev&quot;)]/managedEntity[(@name=&quot;AppDev_Anne&quot;)]/sampler[(@name=&quot;Root Disk A&quot;)][(@type=&quot;GeneralCheck_A&quot;)]/dataview[(@name=&quot;Root Disk A&quot;)]/rows/row[(@name=&quot;/&quot;)]/cell[(@column=&quot;percentageUsed&quot;)]</target>
            </targets>
            <priority>1</priority>
            <block>
                <if>
                    <gt>
                        <dataItem>
                            <property>@value</property>
                        </dataItem>
                        <integer>79</integer>
                    </gt>
                    <transaction>
                        <update>
                            <property>state/@severity</property>
                            <severity>critical</severity>
                        </update>
                    </transaction>
                    <transaction>
                        <update>
                            <property>state/@severity</property>
                            <severity>ok</severity>
                        </update>
                    </transaction>
                </if>
            </block>
        </rule>
    </rules>
    <activeTimes>
        <activeTime name="NAM Hours">
            <scheduledPeriod>
                <startTime>08:00:00</startTime>
                <endTime>05:00:00</endTime>
            </scheduledPeriod>
        </activeTime>
    </activeTimes>
</gateway>